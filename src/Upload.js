const { v4: uuidv4 } = require("uuid");
const AWS = require("aws-sdk");

const imageUploadBucket = "aws-homeplus-avatar-uploader-bucket-dev";
const region = "us-east-1";
const accessKeyId = "AKIA33DEMBZDBCI33BPD";
const secretAccessKey = "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";

const s3 = new AWS.S3({
  region,
  accessKeyId,
  secretAccessKey,
  signatureVersion: "v4",
});

const allowedMimes = ["image/jpeg", "image/png", "image/jpg"];

const Upload = async ({ image, mime }) => {
  const response = {
    imageName: null,
    statusCode: 200,
    body: JSON.stringify({ message: "Successfully uploaded file to S3" }),
  };

  try {
    // const body = JSON.parse(event.body);

    if (!image || !mime) {
      response.statusCode = 400;
      response.body = JSON.stringify({ message: "incorrect body on request" });
      return response;
    }

    if (!allowedMimes.includes(mime)) {
      response.statusCode = 400;
      response.body = JSON.stringify({ message: "mime is not allowed" });
      return response;
    }

    let imageData = image;
    if (image.substr(0, 7) === "base64,") {
      imageData = image.substr(7, image.length);
    }

    const buffer = Buffer.from(imageData, "base64");
    // const fileInfo = await fileType.fromBuffer(buffer);
    // const detectedExt = fileInfo.ext;
    // const detectedMime = fileInfo.mime;

    if (detectedMime !== mime) {
      response.statusCode = 400;
      response.body = JSON.stringify({ message: "mime types dont match" });
      return response;
    }

    const name = uuidv4();
    const key = `${name}.${mime}`;

    console.log(`writing image to bucket called ${key}`);

    await s3
      .putObject({
        Body: buffer,
        Key: key,
        ContentType: mime,
        Bucket: imageUploadBucket,
        ACL: "public-read",
      })
      .promise();

    const url = `https://${imageUploadBucket}.s3-${region}.amazonaws.com/${key}`;

    response.imageName = key;
    return response;
  } catch (error) {
    console.log("error", error);
    response.statusCode = 400;
    response.body = JSON.stringify({
      message: error.message || "failed to upload image",
    });
    return response;
  }
};

export default Upload;
