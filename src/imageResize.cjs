import S3 from "./common/S3.cjs";
import jimp from "jimp";

module.exports.handler = async (event) => {
  const { Records } = event;
  const response = {
    imageName: null,
    statusCode: 200,
    body: JSON.stringify({ message: "Successfully uploaded file to S3" }),
  };
  try {
    const promArray = Records.map((record) => {
      const bucket = record.s3.bucket.name;
      const file = record.s3.bucket.key;
      const width = 300;
      const height = 300;
      return resizeImage({ bucket, file, width, height });
    });

    await Promise.all(promArray);

    return response;
  } catch (error) {
    console.log("error in try catch", error);
    response.statusCode = 400;
    response.body = JSON.stringify({ message: error.message });
    return response;
  }
};

const resizeImage = async ({ bucket, file, width, height }) => {
  const imageBuffer = await S3.get(file, bucket);
  const jimpImage = await jimp.read(imageBuffer.Body);
  const mime = jimpImage.getMIME();

  const resizedImageBuffer = await jimpImage
    .scaleToFit(width, height)
    .getBufferAsync(mime);

  const shortFileName = file.split("/")[1];
  const newFileName = `resized/${width}x${height}/${shortFileName}`;

  await S3.write(resizedImageBuffer, newFileName, bucket, "public-read", mime);
  return newFileName;
};
