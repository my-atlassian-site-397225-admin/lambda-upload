const { v4: uuidv4 } = require("uuid");
const AWS = require("aws-sdk");
const s3 = new AWS.S3();

const allowedMimes = ["image/jpeg", "image/png", "image/jpg"];

const imageUploadBucket = "aws-homeplus-avatar-uploader-bucket-dev";
const region = "us-east-1";

module.exports.handler = async (event) => {
  const response = {
    imageName: null,
    statusCode: 200,
    body: JSON.stringify({ message: "Successfully uploaded file to S3" }),
  };

  try {
    const body = JSON.parse(event.body);

    if (!body || !body.image || !body.mime) {
      response.statusCode = 400;
      response.body = JSON.stringify({ message: "incorrect body on request" });
      return response;
    }

    if (!allowedMimes.includes(body.mime)) {
      response.statusCode = 400;
      response.body = JSON.stringify({ message: "mime is not allowed" });
      return response;
    }

    let imageData = body.image;
    if (body.image.substr(0, 7) === "base64,") {
      imageData = body.image.substr(7, body.image.length);
    }

    const buffer = Buffer.from(imageData, "base64");
    // const fileInfo = await fileType.fromBuffer(buffer);
    const detectedExt = "JPEG";
    const detectedMime = "image/jpeg";

    if (detectedMime !== body.mime) {
      response.statusCode = 400;
      response.body = JSON.stringify({ message: "mime types dont match" });
      return response;
    }

    const name = uuidv4();
    const key = `${name}.${detectedExt}`;

    console.log(`writing image to bucket called ${key}`);

    await s3
      .putObject({
        Body: buffer,
        Key: key,
        ContentType: body.mime,
        Bucket: imageUploadBucket,
        ACL: "public-read",
      })
      .promise();

    const url = `https://${imageUploadBucket}.s3-${region}.amazonaws.com/${key}`;

    response.imageName = key;
    return response;
  } catch (error) {
    console.log("error", error);
    response.statusCode = 400;
    response.body = JSON.stringify({
      message: error.message || "failed to upload image",
    });
    return response;
  }
};
